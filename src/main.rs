use macroquad::prelude::*;

fn window_conf() -> Conf {
    Conf {
        window_title: "Egui Macroquad Template".to_owned(),
        high_dpi: true,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    set_camera(&Camera2D {
        zoom: vec2(1. / 20. * 2., -1. / 20. * 2.),
        target: vec2(20. / 2., 20. / 2.),
        ..Default::default()
    });
    loop {
        clear_background(WHITE);

        // Process keys, mouse etc.

        egui_macroquad::ui(|egui_ctx| {
            egui::Window::new("egui ❤ macroquad")
                .show(egui_ctx, |ui| {
                    ui.label("Window content");
                });
        });

        // Draw things before egui

        egui_macroquad::draw();

        // Draw things after egui

        next_frame().await;
    }
}
